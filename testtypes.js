// script d'ajout de champ 'text' aux types docs pour utilisation dans select2

var fs = require('fs');
var config = require('./testtypes.json');
config.types.forEach((type, index, array) => {
    type.text = type.label;
    console.log(type.label);
});

fs.writeFile('./testtypes2.json', JSON.stringify(config), function (err) {
    if (err) return console.log(err);
    console.log('data > ./testtypes2.json');
  });