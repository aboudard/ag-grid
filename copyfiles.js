var copyfiles = require('copyfiles');
//copyfiles(['src/**/*.js', 'src/style.css', 'src/index.html', 'dist/'], (e) => {
//    if (e) console.error(e);
//});
copyfiles([
    './node_modules/ag-grid-community/dist/ag-grid-community.js',
    './node_modules/ag-grid-community/dist/styles/ag-grid.css',
    './node_modules/ag-grid-community/dist/styles/ag-theme-balham.css',
    './node_modules/select2/dist/css/select2.min.css',
    './node_modules/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css',
    './node_modules/dropzone/dist/basic.css',
    './node_modules/dropzone/dist/dropzone.css',
    './node_modules/bootstrap/dist/css/bootstrap.min.css',
    './node_modules/@fortawesome/fontawesome-free/js/brands.js',
    './node_modules/@fortawesome/fontawesome-free/js/solid.js',
    './node_modules/@fortawesome/fontawesome-free/js/regular.js',
    './node_modules/@fortawesome/fontawesome-free/js/fontawesome.js',
    './node_modules/jquery/dist/jquery.js',
    './node_modules/select2/dist/js/select2.js',
    './node_modules/dropzone/dist/dropzone.js',
    './node_modules/bootstrap/js/dist/util.js',
    './node_modules/bootstrap/js/dist/tab.js',
    './node_modules/bootstrap/js/dist/collapse.js',
    './node_modules/bootstrap/js/dist/modal.js',
    'src/'], (e) => {
        if (e) console.error(e);
    });