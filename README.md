# module pièces jointes

Le script de build permet de copier les fichiers des dépendances.
Quand on ajoute une dépendance avec npm install et que l'on ajoute cette librairie dans le fichier index.html, il faut également l'ajouter dans le script de build :
- copyfiles.js
## Lancement mode dev
- npm i
- npm run build
- npm run server
- npm run front
