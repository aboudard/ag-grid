class ExtensionRenderer {
  init(agParams) {
    this.agParams = agParams;
    let icon = "file";
    let color = "#01afec";
    if (this.agParams.data.extension === "pdf") {
      icon = "file-pdf";
      color = "#db042a";
    } else if (this.agParams.data.extension === "xls") {
      icon = "file-excel";
    } else if (
      this.agParams.data.extension === "png" ||
      this.agParams.data.extension === "jpg"
    ) {
      icon = "file-image";
      color = "#92d050";
    }

    this.eGui = document.createElement("span");
    this.eGui.setAttribute("title", "Afficher le document");
    this.eGui.style.cursor = "pointer";
    this.eGui.innerHTML = `<i style="color:${color};" class="fas fa-${icon}"></i>`;
    this.btnClickedHandler = this.btnClickedHandler.bind(this);
    this.eGui.addEventListener("click", this.btnClickedHandler);
  }
  getGui() {
    return this.eGui;
  }
  btnClickedHandler() {
    this.agParams.clicked(this.agParams.node.data);
  }
}
