class BtnCellRenderer {
  init(agParams) {
    this.agParams = agParams;
    // pas de bouton pour action cut et fichier non pdf
    if (this.agParams.data.extension !== "pdf" && this.agParams.type === "pdf") {
      return;
    }
    if (this.agParams.data.favori === true && this.agParams.type === "star") {
      this.agParams.iconClass = "far fa-star";
    }
    this.eGui = document.createElement("span");
    this.eGui.style.cursor = "pointer";
    this.eGui.innerHTML = `<i style="color:${this.agParams.color};" class="${this.agParams.iconClass}"></i>`;

    this.btnClickedHandler = this.btnClickedHandler.bind(this);
    this.eGui.addEventListener("click", this.btnClickedHandler);
  }
  getGui() {
    return this.eGui;
  }
  btnClickedHandler() {
    this.agParams.clicked([this.agParams.node.data]);
  }
}
