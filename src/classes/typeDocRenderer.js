class TypeDocRenderer {
  init(agParams) {
    this.agParams = agParams;
    this.eGui = document.createElement("div");
    this.eGui.setAttribute("title", "Type document");
    this.eGui.style.cursor = "pointer";
    const t = typesDocs.concat(typesDocsConf).find(t => t.id === this.agParams.data.typedocId);
    const iconConf = t?.confidentiel ? '<i class="fa fa-lock mr-2"></i>' : '';
    this.eGui.innerHTML = iconConf + t.aDocLabel;
    this.btnClickedHandler = this.btnClickedHandler.bind(this);
    this.eGui.addEventListener("click", this.btnClickedHandler);
  }
  getGui() {
    return this.eGui;
  }
  btnClickedHandler() {
    this.agParams.clicked(this.agParams.node.data);
  }
}
