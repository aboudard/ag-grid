class CheckboxRenderer {
  init(agParams) {
    this.agParams = agParams;
    this.eGui = document.createElement("input");
    // this.eGui.classList.add('ag-input-field-input');
    // this.eGui.classList.add('ag-checkbox-input');
    this.eGui.type = "checkbox";
    this.eGui.checked = this.agParams.value;
    this.checkedHandler = this.checkedHandler.bind(this);
    this.eGui.addEventListener("click", this.checkedHandler);
  }
  getGui() {
    return this.eGui;
  }
  checkedHandler(e) {
    let checked = e.target.checked;
    let colId = this.agParams.column.colId;
    this.agParams.node.setDataValue(colId, checked);
  }
}
