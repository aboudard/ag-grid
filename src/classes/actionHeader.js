class ActionHeader {
    init(agParams) {
        this.agParams = agParams;
        this.eGui = document.createElement('span');
        this.eGui.setAttribute('title', this.agParams.title);
        this.eGui.style.cursor = 'pointer';
        this.eGui.innerHTML = `<i style="color:${this.agParams.color};" class="${this.agParams.iconClass}"></i>`;
        this.btnClickedHandler = this.btnClickedHandler.bind(this);
        this.eGui.addEventListener('click', this.btnClickedHandler);
    }
    getGui() {
        return this.eGui;
      }
      btnClickedHandler() {
        this.agParams.clicked();
      }
}