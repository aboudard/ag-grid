  // specify the columns
  const columnSupprDefs = [
    {
        maxWidth: 30,
        checkboxSelection: true,
        headerCheckboxSelection: true,
        filter: false,
        sortable: false
      },
    { field: "titre", minWidth: 200 },
    { field: "nomAssocie" },
    { field: "typedocId"},
    {
      field: "size",
      headerName: "Taille",
      maxWidth: 100,
    },
    {
        headerComponent: ActionHeader,
        headerComponentParams: {
            color: "#01afec",
            title: "Restaurer sélection",
            iconClass: "fas fa-sync-alt",
            clicked: () => restaureDocs(gridSupprOptions.api.getSelectedRows())
        },
        cellRenderer: BtnCellRenderer, 
        cellRendererParams: {
          type: '',  
          iconClass: 'fas fa-sync-alt',
            color: '#01afec',
            clicked: restaureDocs // le renderer injecte le document courant
        },
        minWidth: 30,
        filter: false,
        sortable: false
    }
];

  // let the grid know which columns to use
  const gridSupprOptions = {
    columnDefs: columnSupprDefs,
    rowSelection: 'multiple',
    suppressRowClickSelection: true,
    onGridReady: (p) => {
      console.log("grid ready");
    }
  };

  // TODO: implémenter l'appel de service
  function restaureDocs(docs) {
    if (!docs.length) {
      window.alert("Aucun document sélectionné");
    } else {
      // appel de service de restauration
      gridSupprOptions.api.applyTransaction({ remove: docs });
    }
  }