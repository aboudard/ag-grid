// setup the grid after the page has finished loading
document.addEventListener('DOMContentLoaded', function () {
    // lookup the container we want the Grid to use
    const eGridSupprDiv = document.querySelector("#supprGrid");
    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridSupprDiv, gridSupprOptions);

    // fetch the row data to use and one ready provide it to the Grid via the Grid API
    agGrid
    .simpleHttpRequest({ url: "http://localhost:3000/suppr" })
    .then((data) => {
        gridSupprOptions.api.setRowData(data);
    });
});