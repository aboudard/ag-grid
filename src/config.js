// specify the columns
const columnDefs = [
  {
    maxWidth: 30,
    rowDrag: true,
    filter: false,
    sortable: false,
  },
  {
    maxWidth: 30,
    checkboxSelection: true,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    filter: false,
    sortable: false,
  },
  {
    headerComponent: ActionHeader,
    headerComponentParams: {
      color: "#01afec",
      title: "Mettre sélection en favoris",
      iconClass: "far fa-star",
      clicked: () => favoriDocs(gridOptions.api.getSelectedRows()),
    },
    field: "favori",
    maxWidth: 50,
    cellRenderer: BtnCellRenderer,
    cellRendererParams: {
      type: "star",
      iconClass: "fas fa-star",
      color: "#01afec",
      clicked: favoriDocs,
    },
  },
  {
    headerName: "",
    field: "extension",
    cellRenderer: ExtensionRenderer,
    cellRendererParams: {
      clicked: (field) => carrousel(getAllData(), field),
    },
    maxWidth: 50,
    filter: false,
    sortable: false,
  },
  {
    headerName: "Nom du fichier",
    field: "titre",
    minWidth: 300,
  },
  {
    headerName: "Nom associé",
    field: "nomAssocie",
    editable: true,
    minWidth: 300,
  },
  {
    headerName: "Type document",
    field: "typedocId",
    cellRenderer: TypeDocRenderer,
    cellRendererParams: {
      clicked: (field) => typesdoc(field),
    },
    cellStyle: cellTypeDoc,
    minWidth: 190,
    // valueGetter: renderTypeDoc,
  },
  {
    field: "eConsultable",
    cellRenderer: CheckboxRenderer,
  },
  {
    field: "eConsultableCourtier",
    cellRenderer: CheckboxRenderer,
  },
  {
    field: "emisRecu",
    cellRenderer: renderEmisRecu,
    valueGetter: renderEmisRecu,
  },
  {
    field: "dossierPersonne",
    cellRenderer: CheckboxRenderer,
  },
  {
    headerName: "Déjà archivé",
    field: "archive",
    cellRenderer: renderArchive,
    valueGetter: renderArchive,
  },
  {
    field: "size",
    headerName: "Taille",
    maxWidth: 100,
  },
  {
    cellRenderer: BtnCellRenderer,
    cellRendererParams: {
      type: "pdf",
      iconClass: "fas fa-cut",
      color: "#01afec",
      clicked: (field) => cutdoc(field),
    },
    maxWidth: 30,
    filter: false,
    sortable: false,
  },
  {
    headerComponent: ActionHeader,
    headerComponentParams: {
      color: "#db042a",
      title: "Supprimer la sélection",
      iconClass: "fas fa-trash-alt",
      clicked: () => supprDocs(gridOptions.api.getSelectedRows()),
    },
    cellRenderer: BtnCellRenderer,
    cellRendererParams: {
      type: "",
      iconClass: "fas fa-trash-alt",
      color: "#db042a",
      clicked: supprDocs,
    },
    minWidth: 30,
    filter: false,
    sortable: false,
  },
];

// let the grid know which columns to use
const gridOptions = {
  columnDefs: columnDefs,
  defaultColDef: {
    sortable: true,
    filter: true,
  },
  pagination: false,
  paginationPageSize: 10,
  rowDragManaged: true,
  animateRows: true,
  rowSelection: "multiple",
  suppressRowClickSelection: true,
  onCellValueChanged: onCellValueChanged,
  onFirstDataRendered: onFirstDataRendered,
};

function onCellValueChanged(event) {
  console.log(
    "onCellValueChanged: " + event.colDef.field + " = " + event.newValue
  );
}

function onFirstDataRendered(params) {
  params.api.sizeColumnsToFit();
}

// TODO: implémenter
function supprDocs(docs) {
  if (window.confirm("Voulez vous supprimer ce(s) document(s) ?")) {
    console.log(docs);
  }
}

// TODO: implémenter
function favoriDocs(docs) {
  console.log(docs);
}

// TODO: implémenter
function carrousel(docs, selectedDoc) {
  console.log(
    `visionneuse de ${docs.length} docs - doc courant : ${selectedDoc.id}`
  );
}
