// variables globales
let typesDocs = [];
let typesDocsConf = [];
let selectedDoc = null;
const typesDocsUrl = "http://localhost:3000/types";
const typesDocsConfUrl = "http://localhost:3000/confidentiel";
const docsUrl = "http://localhost:3000/docs";

async function fetchTypes() {
  const response = await fetch(typesDocsUrl);
  const types = await response.json();
  return types;
}

async function fetchTypesConf() {
  const response = await fetch(typesDocsConfUrl);
  const types = await response.json();
  return types;
}

function getAllData() {
  let outData = [];
  gridOptions.api.forEachNodeAfterFilter((node) => {
    outData.push(node.data);
  });
  return outData;
}

function getSelectedData() {
  return gridOptions.api.getSelectedRows();
}

function removeAllFilters() {
  gridOptions.api.setFilterModel(null);
  gridOptions.columnApi.applyColumnState({
    defaultState: { sort: null },
  });
}

// TODO: appeler quand on valide le process ?
function allData() {
  console.log(getAllData());
}

function selectedData() {
  console.log(getSelectedData());
}

const templateTypeFct = (data) => {
  // We only really care if there is an element to pull classes from
  if (!data.element) {
    return data.text;
  }

  const confIcon = data.confidentiel ? "fa fa-lock" : "";
  let ic = `<i style="" class="${confIcon} mr-2"></i>`;
  let $wrapper = $(`<span></span>`);
  $wrapper.css("backgroundColor", data.color);
  $wrapper.html(ic + data.text);
  return $wrapper;
};

// remplissage des types docs dans la liste
// lancement de la modal des types docs
function typesdoc(item) {
  if (item)  {
    selectedDoc = item;
  } else if (getSelectedData().length <= 0) {
    window.alert("Aucun document sélectionné");
    return;
  }
  // récupération du domaine
  $("#typesDomaine").val($("#domaine").val());
  resetTypesSelect(typesDocs, $('#domaine').val());
  $("#typesDocsModal").modal("show");
}

// lancement modal découpage docs
function cutdoc(item) {
  selectedDoc = item[0];
  $('#cutDocTitre').text(selectedDoc.titre);
  $("#cutDocsModal").modal("show");
}

/**
 * Gestion des types docs / domaine et confidentialité
 */
const handleTypes = () => {
  const flagConf = $("input[name='typesConfidentiel']:checked").val() === 'true';
  if (flagConf) {
    resetTypesSelect(typesDocsConf, $("#typesDomaine").val());
  } else {
    resetTypesSelect(typesDocs, $("#typesDomaine").val());
  }
};

/*function onPageSizeChanged(newPageSize) {
    var value = document.getElementById('page-size').value;
    gridOptions.api.paginationSetPageSize(Number(value));
  }*/

// setup the grid after the page has finished loading
document.addEventListener("DOMContentLoaded", function () {
  // lookup the container we want the Grid to use
  const eGridDiv = document.querySelector("#docsGrid");

  // create the grid passing in the div to use together with the columns & data we want to use
  new agGrid.Grid(eGridDiv, gridOptions);

  fetchTypesConf().then((typesConf) => {
    typesDocsConf = typesConf;
  });

  // chargement types docs
  fetchTypes().then((types) => {
    // flat la liste des types docs
    typesDocs = flatChildren(types);

    // fetch the row data to use and one ready provide it to the Grid via the Grid API
    agGrid.simpleHttpRequest({ url: docsUrl }).then((data) => {
      gridOptions.api.setRowData(data);
      let initVal = 0;
      let docsSize = data.reduce((accumulator, currentValue) => {
        console.log(
          Number.parseFloat(accumulator) + Number.parseFloat(currentValue.size)
        );
        return (
          Number.parseFloat(accumulator) + Number.parseFloat(currentValue.size)
        );
      }, initVal);
      let maxSize = 300;
      document.getElementById("docsSize").innerHTML = docsSize;
      document.getElementById("maxSize").innerHTML = maxSize;
    });
  });

  // gestion modale types docs
  $("#saveTypesBtn").on("click", function () {
    const id = Number.parseInt($("#typedocId").val());
    if (selectedDoc) {
      selectedDoc.typedocId = id;
    } else {
      for (const item of gridOptions.api.getSelectedRows()) {
        item.typedocId = id;
      }
    }
    gridOptions.api.refreshCells({ force: true });
    $("#typedocId").val(null);
    console.log(id);
    $("#typesDocsModal").modal("hide");
  });

  // gestion modale découpage pdf
  $("#saveCutBtn").on("click", function () {
    const params = $("#pagesCut").val();
    console.log(params);
    $("#cutDocsModal").modal("hide");
  });

  $("#typesDomaine").on("change", handleTypes);

  $("#typesConfidentielChoix").on("change", handleTypes);

  // The constructor of Dropzone accepts two arguments:
  //
  // 1. The selector for the HTML element that you want to add
  //    Dropzone to, the second
  // 2. An (optional) object with the configuration
  // let myDropzone = new Dropzone("div#uploadDoc", { url: "/file/post"});
});
