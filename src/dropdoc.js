Dropzone.options.docDropzone = { // camelized version of the `id`
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 2, // MB
    accept: (file, done) => {
      if (file.type != 'application/pdf') {
        done('Type de fichier interdit');
      }
      else { done(); }
    },
    init: function() {
        this.on("addedfile", file => {
            console.log(`fichier ajouté ${file.name}`);
          });
    }
  };