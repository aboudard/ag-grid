/*exported renderEmisRecu */
function renderEmisRecu(params) {
  if (params.data.emisRecu === "0") {
    return 'Oui';
  } else if (params.data.emisRecu === "1") {
    return 'Non';
  } else {
    return params.data.emisRecu;
  }
}

function renderArchive(params) {
    return params.data.archive ? "Oui" : "Non";
}

function cellTypeDoc(params) {
    const t = typesDocs.concat(typesDocsConf).find(t => t.id === params.data.typedocId);
    return t ? { backgroundColor: t.color } : {};
}
