/**
 * Aplatir un tableau d'objets contenant des children
 * @param {*} tab: le tableau à aplatir
 * @returns un nouveau tableau
 */
const flatChildren = (tab) =>
  tab.reduce((flattened, obj) => {
    return flattened
      .concat([obj])
      .concat(obj.children.length ? flatChildren(obj.children) : []);
  }, []);

/**
 * Purge des champs inutiles, filtre des types docs sur le domaine courant
 * @param {*} tab: le tableau des types docs
 * @param {*} domaine: le domaine courant
 * @returns un nouveau tableau
 */
const filterTypesDocsByDomaine = (tab, domaine) => {
  var data = [];
  data = tab.flatMap((item) => {
    return item.domaine === domaine
      ? [
          {
            id: item.id,
            text: item.label,
            color: item.color,
            confidentiel: item.confidentiel,
          },
        ]
      : [];
  });
  return data;
};

const resetTypesSelect = (types, domaine) => {
  $('#typedocId').val(null).trigger('change');
  $('#typedocId').empty().trigger('change');
  $('#typedocId').select2({
    theme: 'bootstrap4',
    data: filterTypesDocsByDomaine(types, domaine),
    templateResult: templateTypeFct,
    templateSelection: templateTypeFct
}).trigger('change');
// hack pour corriger le resize auto du select ...
$('.select2-container').css('width', '');
};
