// setup the grid after the page has finished loading
document.addEventListener('DOMContentLoaded', function () {
    // lookup the container we want the Grid to use
    const eGridActionsDiv = document.querySelector("#actionsGrid");
    // create the grid passing in the div to use together with the columns & data we want to use
    new agGrid.Grid(eGridActionsDiv, gridActionsOptions);

    // fetch the row data to use and one ready provide it to the Grid via the Grid API
    agGrid
    .simpleHttpRequest({ url: "http://localhost:3000/actions" })
    .then((data) => {
        gridActionsOptions.api.setRowData(data);
    });
});