  // specify the columns
  const columnActionsDefs = [
    {
      headerName: "Nom du fichier",
      field: "nom_fichier",
    },
    {
      field: "action",
    },
    {
      field: "date",
      sortable: true
    },
    {
      field: "badge",
    }
];

  // let the grid know which columns to use
  const gridActionsOptions = {
    columnDefs: columnActionsDefs,
    onGridReady: () => {
      console.log("grid ready");
    },
    pagination: true,
    paginationPageSize: 10,
  };