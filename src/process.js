const reportWindowSize = () => {
    gridOptions.api.sizeColumnsToFit();
    gridActionsOptions.api.sizeColumnsToFit();
    gridSupprOptions.api.sizeColumnsToFit();
};

const onTypeProcessChanged = (e) => {
    const checkVie = (e.target.value === '1');
    const checkIard = (e.target.value === '0');
    // cacher les colonnes en fonction du "domaine" IARD / VIE
  gridOptions.columnApi.setColumnsVisible(['eConsultable', 'eConsultableCourtier'], checkVie);
  gridOptions.columnApi.setColumnsVisible(['dossierPersonne', 'emisRecu'], checkIard);
  gridOptions.api.sizeColumnsToFit();
};

document.addEventListener('DOMContentLoaded', function () {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
        console.log(event.target.id); // newly activated tab
        // event.relatedTarget // previous active tab
        if (event.target.id === 'home-tab') {
            gridOptions.api.sizeColumnsToFit();
        } else if (event.target.id === 'actions-tab') {
            gridActionsOptions.api.sizeColumnsToFit();
        } else if (event.target.id === 'suppr-tab') {
            gridSupprOptions.api.sizeColumnsToFit();
        }
      });
      document.querySelector('#type_process').addEventListener('change', onTypeProcessChanged);
      // initialisation du domaine
      onTypeProcessChanged({target: {value: '1'}});

      $('#collapseExample').on('hidden.bs.collapse', () => {
        console.log('collapse');
      });
      $('#collapseExample').on('shown.bs.collapse', () => {
        console.log('shown');
      })

});

window.addEventListener('resize', reportWindowSize);

